package com.allstate.crm.search.controller;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.CustomerDetails;
import com.allstate.crm.entities.Policy;
import org.json.JSONException;
import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CustomerSearchControllerTest {

    private int port=8888;
    private AutoCloseable closeable;

    @Autowired
    TestRestTemplate restTemplate;
    HttpHeaders headers = new HttpHeaders();
    Customer customer;

    @BeforeEach
    public  void openMocks() {
        closeable = MockitoAnnotations.openMocks(this);
        customer = new Customer(12345, "kumar", "illa", 35, "c 306 vaishno apratment, hoodi",
                "bangalore", "karnataka", "9874321", "ailla@allstate1.com", "male",
                "1234-5678-9876", "Spanish", Arrays.asList("9876541344"));
    }

    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }

    @Test
    public void fetchCustomersByPhoneNumber() throws JSONException {
        HttpEntity<Customer> customerHttpEntity = new HttpEntity<Customer>(customer, headers);

        ResponseEntity<List<Customer>> response = restTemplate.exchange("/crm/customersearch/phonenumber/987654321",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Customer>>() {
                } , "987654321"
        );
         assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void fetchCustomersByEmail() throws JSONException {
        HttpEntity<Customer> customerHttpEntity = new HttpEntity<Customer>(customer, headers);

        ResponseEntity<List<Customer>> response = restTemplate.exchange("/crm/customersearch/email/ailla@allstate1.com",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Customer>>() {
                } , "ailla@allstate1.com"
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void fetchCustomersByFirstName() throws JSONException {
        HttpEntity<Customer> customerHttpEntity = new HttpEntity<Customer>(customer, headers);

        ResponseEntity<List<Customer>> response = restTemplate.exchange("/crm/customersearch/firstname/kumar",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Customer>>() {
                } , "kumar"
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void fetchCustomersByPolicyNumber() throws JSONException {
        HttpEntity<Customer> customerHttpEntity = new HttpEntity<Customer>(customer, headers);

        ResponseEntity<List<Customer>> response = restTemplate.exchange("/crm/customersearch/policynumber/9876541344",
                HttpMethod.GET, null, new ParameterizedTypeReference<List<Customer>>() {
                } , "9876541344"
        );
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @After
    public void releaseMocks() throws Exception {
        closeable.close();
    }
}
