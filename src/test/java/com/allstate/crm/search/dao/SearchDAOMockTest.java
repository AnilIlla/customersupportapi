package com.allstate.crm.search.dao;

import com.allstate.crm.entities.Customer;
import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.doReturn;

public class SearchDAOMockTest {


    @Mock
    private SearchDAO searchDAO;

    private Customer customer;

    private AutoCloseable closeable;


    @BeforeEach
    public  void openMocks() {
        closeable = MockitoAnnotations.openMocks(this);
        customer = new Customer(12345, "kumar", "illa", 35, "c 306 vaishno apratment, hoodi",
                "bangalore", "karnataka", "987654321", "ailla@allstate1.com", "male",
                "1234-5678-9876", "Spanish", Arrays.asList("9876541344"));
    }

    @After
    public void releaseMocks() throws Exception {
        closeable.close();
    }

    @Test
    public void findCustomersByPhoneNumber_Success_Test() {
        List<Customer> customers = new ArrayList<Customer>();
        customers.add(customer);
        doReturn(customers).when(searchDAO).findCustomersByPhoneNumber(anyString());
        assertEquals(customers.get(0).getId(), customer.getId());
        assertEquals(customers.get(0).getFirstName(), customer.getFirstName());
        assertEquals(customers.get(0).getLastName(), customer.getLastName());
        assertEquals(customers.get(0).getAge(), customer.getAge());
        assertEquals(customers.get(0).getAddress(), customer.getAddress());
        assertEquals(customers.get(0).getState(), customer.getState());
        assertEquals(customers.get(0).getPhone(), customer.getPhone());
        assertEquals(customers.get(0).getEmail(), customer.getEmail());
        assertEquals(customers.get(0).getGender(), customer.getGender());
        assertEquals(customers.get(0).getSsn(), customer.getSsn());
        assertEquals(customers.get(0).getLanguage(), customer.getLanguage());
        assertEquals(customers.get(0).getPolicies().get(0),customer.getPolicies().get(0));
    }

    @Test
    public void findCustomersByPhoneNumber_Failure_Test(){
        List<Customer> customers = new ArrayList<Customer>();
        doReturn(customers).when(searchDAO).findCustomersByPhoneNumber("345678923456");
        assertEquals(customers.size(), 0);
    }


    @Test
    public void findCustomersByFirstName_Success_Test() {
        List<Customer> customers = new ArrayList<Customer>();
        customers.add(customer);
        doReturn(customers).when(searchDAO).findCustomersByFirstName(anyString());
        assertEquals(customers.get(0).getId(), customer.getId());
        assertEquals(customers.get(0).getFirstName(), customer.getFirstName());
        assertEquals(customers.get(0).getLastName(), customer.getLastName());
        assertEquals(customers.get(0).getAge(), customer.getAge());
        assertEquals(customers.get(0).getAddress(), customer.getAddress());
        assertEquals(customers.get(0).getState(), customer.getState());
        assertEquals(customers.get(0).getPhone(), customer.getPhone());
        assertEquals(customers.get(0).getEmail(), customer.getEmail());
        assertEquals(customers.get(0).getGender(), customer.getGender());
        assertEquals(customers.get(0).getSsn(), customer.getSsn());
        assertEquals(customers.get(0).getLanguage(), customer.getLanguage());
        assertEquals(customers.get(0).getPolicies().get(0),customer.getPolicies().get(0));
    }

    @Test
    public void findCustomersByFirstName_Failure_Test(){
        List<Customer> customers = new ArrayList<Customer>();
        doReturn(customers).when(searchDAO).findCustomersByFirstName("abcdef");
        assertEquals(customers.size(), 0);
    }


    @Test
    public void findCustomersByEmail_Success_Test() {
        List<Customer> customers = new ArrayList<Customer>();
        customers.add(customer);
        doReturn(customers).when(searchDAO).findCustomersByEmail(anyString());
        assertEquals(customers.get(0).getId(), customer.getId());
        assertEquals(customers.get(0).getFirstName(), customer.getFirstName());
        assertEquals(customers.get(0).getLastName(), customer.getLastName());
        assertEquals(customers.get(0).getAge(), customer.getAge());
        assertEquals(customers.get(0).getAddress(), customer.getAddress());
        assertEquals(customers.get(0).getState(), customer.getState());
        assertEquals(customers.get(0).getPhone(), customer.getPhone());
        assertEquals(customers.get(0).getEmail(), customer.getEmail());
        assertEquals(customers.get(0).getGender(), customer.getGender());
        assertEquals(customers.get(0).getSsn(), customer.getSsn());
        assertEquals(customers.get(0).getLanguage(), customer.getLanguage());
        assertEquals(customers.get(0).getPolicies().get(0),customer.getPolicies().get(0));
    }

    @Test
    public void findCustomersByEmail_Failure_Test(){
        List<Customer> customers = new ArrayList<Customer>();
        doReturn(customers).when(searchDAO).findCustomersByPhoneNumber("abcd.allstate.com");
        assertEquals(customers.size(), 0);
    }


    @Test
    public void findCustomersByPolicyNumber_Success_Test() {
        List<Customer> customers = new ArrayList<Customer>();
        customers.add(customer);
        doReturn(customers).when(searchDAO).findCustomersByPolicyNumber(anyString());
        assertEquals(customers.get(0).getId(), customer.getId());
        assertEquals(customers.get(0).getFirstName(), customer.getFirstName());
        assertEquals(customers.get(0).getLastName(), customer.getLastName());
        assertEquals(customers.get(0).getAge(), customer.getAge());
        assertEquals(customers.get(0).getAddress(), customer.getAddress());
        assertEquals(customers.get(0).getState(), customer.getState());
        assertEquals(customers.get(0).getPhone(), customer.getPhone());
        assertEquals(customers.get(0).getEmail(), customer.getEmail());
        assertEquals(customers.get(0).getGender(), customer.getGender());
        assertEquals(customers.get(0).getSsn(), customer.getSsn());
        assertEquals(customers.get(0).getLanguage(), customer.getLanguage());
        assertEquals(customers.get(0).getPolicies().get(0),customer.getPolicies().get(0));
    }

    @Test
    public void findCustomersByPolicyNumber_Failure_Test(){
        List<Customer> customers = new ArrayList<Customer>();
        doReturn(customers).when(searchDAO).findCustomersByPhoneNumber("INC4214684257");
        assertEquals(customers.size(), 0);
    }
}
