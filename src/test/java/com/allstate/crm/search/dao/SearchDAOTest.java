package com.allstate.crm.search.dao;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.search.repository.CustomerMongoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class SearchDAOTest {
    @Autowired
    private CustomerMongoRepository customerMongoRepository;

    @Autowired
    private SearchDAO searchDAO;

    @BeforeEach
    public void cleanupAndReset() {
        customerMongoRepository.delete(new Customer(12345, "kumar", "illa", 35, "c 306 vaishno apratment, hoodi",
                "bangalore", "karnataka", "987654321", "ailla@allstate1.com", "male", "1234-5678-9876", "Spanish", Arrays.asList("9876541344")));
        customerMongoRepository.save(new Customer(12345, "kumar", "illa", 35, "c 306 vaishno apratment, hoodi",
                "bangalore", "karnataka", "987654321", "ailla@allstate1.com", "male",
                "1234-5678-9876", "Spanish", Arrays.asList("9876541344")));
    }

    @Test
    public void findCustomersByFirstNameTest() {
        List<Customer> customers = searchDAO.findCustomersByFirstName("kumar");
        for (Customer customer : customers) {
            assertEquals("kumar", customer.getFirstName());
        }
    }

    @Test
    public void findCustomersByPolicyNumber() {
        List<Customer> customers = searchDAO.findCustomersByPolicyNumber("9876541344");
        for (Customer customer : customers) {
            assertEquals("9876541344", (String) customer.getPolicies().get(0));
        }
    }

    @Test
    public void findCustomersByPhoneNumber() {
        List<Customer> customers = searchDAO.findCustomersByPolicyNumber("987654321");
        for (Customer customer : customers) {
            assertEquals("987654321", customer.getPhone());
        }
    }

    @Test
    public void findCustomersByEmail() {
        List<Customer> customers = searchDAO.findCustomersByEmail("ailla@allstate1.com");
        for (Customer customer : customers) {
            assertEquals("ailla@allstate1.com", customer.getEmail());
        }
    }

}
