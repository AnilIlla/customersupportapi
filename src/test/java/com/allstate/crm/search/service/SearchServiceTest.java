package com.allstate.crm.search.service;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.search.exceptions.InvalidServiceRequestException;
import com.allstate.crm.search.exceptions.SearchServiceException;
import com.allstate.crm.search.dao.SearchDAO;
import com.allstate.crm.utility.ValidationHelper;
import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;

@SpringBootTest
public class SearchServiceTest {
    @Autowired
    SearchService searchService;

    @MockBean
    SearchDAO searchDAO;

    @MockBean
    ValidationHelper validation;

    private AutoCloseable closeable;

    private Customer customer;

    @BeforeEach
    public  void openMocks() {
        closeable = MockitoAnnotations.openMocks(this);
        customer = new Customer(12345, "kumar", "illa", 35, "c 306 vaishno apratment, hoodi",
                "bangalore", "karnataka", "987654321", "ailla@allstate1.com", "male",
                "1234-5678-9876", "Spanish", Arrays.asList("9876541344"));
    }

    @Test
    public void getCustomersByPhoneNumber_Success_Test() {
        List<Customer> customers = new ArrayList<Customer>();
        customers.add(customer);
        doReturn(customers).when(searchDAO).findCustomersByPhoneNumber(anyString());
        assertEquals(1, searchService.getCustomersByPhoneNumber("987654321").size());
        assertEquals(customers.get(0).getId(), customer.getId());
        assertEquals(customers.get(0).getFirstName(), customer.getFirstName());
        assertEquals(customers.get(0).getLastName(), customer.getLastName());
        assertEquals(customers.get(0).getAge(), customer.getAge());
        assertEquals(customers.get(0).getAddress(), customer.getAddress());
        assertEquals(customers.get(0).getState(), customer.getState());
        assertEquals(customers.get(0).getPhone(), customer.getPhone());
        assertEquals(customers.get(0).getEmail(), customer.getEmail());
        assertEquals(customers.get(0).getGender(), customer.getGender());
        assertEquals(customers.get(0).getSsn(), customer.getSsn());
        assertEquals(customers.get(0).getLanguage(), customer.getLanguage());
        assertEquals(customers.get(0).getPolicies().get(0),customer.getPolicies().get(0));
    }

    @Test
    public void getCustomersByPhoneNumber_Failure_Test(){
        doThrow(new RuntimeException()).when(searchDAO).findCustomersByPhoneNumber("987654321");
        SearchServiceException exception = assertThrows(SearchServiceException.class, () -> {
            searchService.getCustomersByPhoneNumber("987654321");
        });
        String expectedMessage = "Search Service Currently Experiencing Issues";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void getCustomersByPhoneNumber_InvalidPhoneFailure_Test(){
        doReturn(new ArrayList<Customer>()).when(searchDAO).findCustomersByPhoneNumber(anyString());
        doReturn(true).when(validation).isEmptyString(anyString());
        InvalidServiceRequestException exception = assertThrows(InvalidServiceRequestException.class, () -> {
            searchService.getCustomersByPhoneNumber("ailla@allstate1.com");
        });
        String expectedMessage = "PhoneNumber Cannot be Null/Empty";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    public void getCustomersByFirstName_Success_Test() {
        List<Customer> customers = new ArrayList<Customer>();
        customers.add(customer);
        doReturn(customers).when(searchDAO).findCustomersByFirstName(anyString());
        assertEquals(1, searchService.getCustomersByFirstName("kumar").size());
        assertEquals(customers.get(0).getId(), customer.getId());
        assertEquals(customers.get(0).getFirstName(), customer.getFirstName());
        assertEquals(customers.get(0).getLastName(), customer.getLastName());
        assertEquals(customers.get(0).getAge(), customer.getAge());
        assertEquals(customers.get(0).getAddress(), customer.getAddress());
        assertEquals(customers.get(0).getState(), customer.getState());
        assertEquals(customers.get(0).getPhone(), customer.getPhone());
        assertEquals(customers.get(0).getEmail(), customer.getEmail());
        assertEquals(customers.get(0).getGender(), customer.getGender());
        assertEquals(customers.get(0).getSsn(), customer.getSsn());
        assertEquals(customers.get(0).getLanguage(), customer.getLanguage());
        assertEquals(customers.get(0).getPolicies().get(0),customer.getPolicies().get(0));
    }

    @Test
    public void getCustomersByFirstName_Failure_Test(){
        doThrow(new RuntimeException()).when(searchDAO).findCustomersByFirstName("kumar");
        SearchServiceException exception = assertThrows(SearchServiceException.class, () -> {
            searchService.getCustomersByFirstName("kumar");
        });
        String expectedMessage = "Search Service Currently Experiencing Issues";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void getCustomersByFirstName_InvalidName_Failure_Test(){
        doReturn(new ArrayList<Customer>()).when(searchDAO).findCustomersByFirstName(anyString());
        doReturn(true).when(validation).isEmptyString(anyString());
        InvalidServiceRequestException exception = assertThrows(InvalidServiceRequestException.class, () -> {
            searchService.getCustomersByFirstName("ailla@allstate1.com");
        });
        String expectedMessage = "First Name Cannot be Null/Empty";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    public void getCustomersByEmail_Success_Test() {
        List<Customer> customers = new ArrayList<Customer>();
        customers.add(customer);
        doReturn(customers).when(searchDAO).findCustomersByEmail(anyString());
        doReturn(true).when(validation).isValidEmail(anyString());
        assertEquals(1, searchService.getCustomersByEmail("ailla@allstate1.com").size());
        assertEquals(customers.get(0).getId(), customer.getId());
        assertEquals(customers.get(0).getFirstName(), customer.getFirstName());
        assertEquals(customers.get(0).getLastName(), customer.getLastName());
        assertEquals(customers.get(0).getAge(), customer.getAge());
        assertEquals(customers.get(0).getAddress(), customer.getAddress());
        assertEquals(customers.get(0).getState(), customer.getState());
        assertEquals(customers.get(0).getPhone(), customer.getPhone());
        assertEquals(customers.get(0).getEmail(), customer.getEmail());
        assertEquals(customers.get(0).getGender(), customer.getGender());
        assertEquals(customers.get(0).getSsn(), customer.getSsn());
        assertEquals(customers.get(0).getLanguage(), customer.getLanguage());
        assertEquals(customers.get(0).getPolicies().get(0),customer.getPolicies().get(0));
    }

    @Test
    public void getCustomersByEmail_Failure_Test(){
        doThrow(new RuntimeException()).when(searchDAO).findCustomersByEmail(anyString());
        doReturn(true).when(validation).isValidEmail(anyString());
        doReturn(false).when(validation).isEmptyString(anyString());
        SearchServiceException exception = assertThrows(SearchServiceException.class, () -> {
            searchService.getCustomersByEmail("ailla@allstate1.com");
        });
        String expectedMessage = "Search Service Currently Experiencing Issues";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void getCustomersByEmail_InvalidEmail_Failure_Test(){
        doReturn(new ArrayList<Customer>()).when(searchDAO).findCustomersByEmail(anyString());
        doReturn(false).when(validation).isValidEmail(anyString());
        InvalidServiceRequestException exception = assertThrows(InvalidServiceRequestException.class, () -> {
            searchService.getCustomersByEmail("ailla@allstate1.com");
        });
        String expectedMessage = "Invalid/Empty Email id";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    public void getCustomersByPolicyNumber_Success_Test() {
        List<Customer> customers = new ArrayList<Customer>();
        customers.add(customer);
        doReturn(customers).when(searchDAO).findCustomersByPolicyNumber(anyString());
        assertEquals(1, searchService.getCustomersByPolicyNumber("9876541344").size());
        assertEquals(customers.get(0).getId(), customer.getId());
        assertEquals(customers.get(0).getFirstName(), customer.getFirstName());
        assertEquals(customers.get(0).getLastName(), customer.getLastName());
        assertEquals(customers.get(0).getAge(), customer.getAge());
        assertEquals(customers.get(0).getAddress(), customer.getAddress());
        assertEquals(customers.get(0).getState(), customer.getState());
        assertEquals(customers.get(0).getPhone(), customer.getPhone());
        assertEquals(customers.get(0).getEmail(), customer.getEmail());
        assertEquals(customers.get(0).getGender(), customer.getGender());
        assertEquals(customers.get(0).getSsn(), customer.getSsn());
        assertEquals(customers.get(0).getLanguage(), customer.getLanguage());
        assertEquals(customers.get(0).getPolicies().get(0),customer.getPolicies().get(0));
    }

    @Test
    public void getCustomersByPolicyNumber_Failure_Test(){
        doThrow(new RuntimeException()).when(searchDAO).findCustomersByPolicyNumber(anyString());
        SearchServiceException exception = assertThrows(SearchServiceException.class, () -> {
            searchService.getCustomersByPolicyNumber("9876541344");
        });
        String expectedMessage = "Search Service Currently Experiencing Issues";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void getCustomersByPolicyNumber_InvalidPolicyNumber_Failure_Test(){
        doReturn(new ArrayList<Customer>()).when(searchDAO).findCustomersByPolicyNumber(anyString());
        doReturn(true).when(validation).isEmptyString(anyString());
        InvalidServiceRequestException exception = assertThrows(InvalidServiceRequestException.class, () -> {
            searchService.getCustomersByPolicyNumber("ailla@allstate1.com");
        });
        String expectedMessage = "PolicyNumber Cannot be Null/Empty";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @After
    public void releaseMocks() throws Exception {
        closeable.close();
    }
}
