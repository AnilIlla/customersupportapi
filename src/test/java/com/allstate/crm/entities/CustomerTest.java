package com.allstate.crm.entities;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomerTest {

            long id = 12345;
            String firstName = "kumar";
            String lastName = "illa";
            int age = 35;
            String address = "c 306 vaishno apratment, hoodi";
            String city = "bangalore";
            String state = "karnataka";
            String phone = "987654321";
            String email = "ailla@allstate1.com";
            String gender = "male";
            String ssn = "1234-5678-9876";
            String language = "Spanish";
            List<String> policies = Arrays.asList("9876541344");



    @Test
    public void constructorInitializesCustomerObjectWithExpectedValues() {
        Customer customer = new Customer(id,firstName,lastName,35,address,city,state,phone,email,gender,ssn,language,policies);
        assertEquals(id, customer.getId());
        assertEquals(firstName, customer.getFirstName());
        assertEquals(lastName, customer.getLastName());
        assertEquals(age, customer.getAge());
        assertEquals(address, customer.getAddress());
        assertEquals(state, customer.getState());
        assertEquals(phone, customer.getPhone());
        assertEquals(email, customer.getEmail());
        assertEquals(gender, customer.getGender());
        assertEquals(ssn, customer.getSsn());
        assertEquals(language, customer.getLanguage());
        assertEquals("9876541344",customer.getPolicies().get(0));


    }
    @Test
    public void setterMethodsInitalizesCustomerObjectWithExpectedValues() {
        Customer customer = new Customer();
        customer.setId(id);
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setAge(age);
        customer.setAddress(address);
        customer.setState(state);
        customer.setPhone(phone);
        customer.setEmail(email);
        customer.setGender(gender);
        customer.setSsn(ssn);
        customer.setLanguage(language);
        customer.setPolicies(policies);
        assertEquals(id, customer.getId());
        assertEquals(firstName, customer.getFirstName());
        assertEquals(lastName, customer.getLastName());
        assertEquals(age, customer.getAge());
        assertEquals(address, customer.getAddress());
        assertEquals(state, customer.getState());
        assertEquals(phone, customer.getPhone());
        assertEquals(email, customer.getEmail());
        assertEquals(gender, customer.getGender());
        assertEquals(ssn, customer.getSsn());
        assertEquals(language, customer.getLanguage());
        assertEquals("9876541344", customer.getPolicies().get(0));

    }
}
