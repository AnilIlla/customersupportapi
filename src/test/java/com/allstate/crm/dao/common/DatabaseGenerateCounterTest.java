package com.allstate.crm.dao.common;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.entities.DatabaseSequence;
import com.allstate.crm.entities.Policy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class DatabaseGenerateCounterTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private DatabaseGenerateCounter dbSequence;

    @BeforeEach
    public void cleanupAndReset() {
        mongoTemplate.dropCollection(DatabaseSequence.class);
    }

    @Test
    public void generateSequenceSuccess()
    {
        String sequence = Customer.SEQUENCE_NAME;
        long counter = dbSequence.generateSequence(sequence);
        assertEquals(1, counter);
    }

    @Test
    public void generateSequencePolicySuccess()
    {
        String sequence = Policy.SEQUENCE_NAME;
        long counter = dbSequence.generateSequence(sequence);
        assertEquals(1, counter);
    }

    @Test
    public void updateSequenceSuccess()
    {
        String sequence = Policy.SEQUENCE_NAME;
        long counter = dbSequence.updateSequence(7,sequence);
        assertEquals(1, counter);
    }
}
