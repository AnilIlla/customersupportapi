package com.allstate.crm.service;

import com.allstate.crm.entities.Policy;

import java.util.List;

public interface PolicyService {
    List<Policy> getPolicyDetailByCustId(int custId);
}
