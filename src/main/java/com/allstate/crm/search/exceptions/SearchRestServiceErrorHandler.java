package com.allstate.crm.search.exceptions;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;
@ControllerAdvice
public class SearchRestServiceErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(InvalidServiceRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public final ResponseEntity<ErrorDetails> handlePaymentNotFound(InvalidServiceRequestException ex, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),ex.getLocalizedMessage(),HttpStatus.BAD_REQUEST.value(),request.getDescription(false));
        return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(SearchServiceException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public final ResponseEntity<ErrorDetails> handleInvalidPaymentRequest(SearchServiceException ex, WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(),ex.getLocalizedMessage(),HttpStatus.INTERNAL_SERVER_ERROR.value(),request.getDescription(false));
        return new ResponseEntity(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(final NoHandlerFoundException ex, final HttpHeaders headers, final HttpStatus status, final WebRequest request) {
        ErrorDetails errorDetails = new ErrorDetails(new Date(),"Requested API resource not found. please check your request URL",ex.getLocalizedMessage(),HttpStatus.NOT_FOUND.value(),ex.getRequestURL());
        return new ResponseEntity(errorDetails,new HttpHeaders(), HttpStatus.NOT_FOUND);
    }
}
