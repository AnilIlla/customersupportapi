package com.allstate.crm.search.exceptions;

public class InvalidServiceRequestException extends RuntimeException {
    public InvalidServiceRequestException(String message) {
        super(message);
    }
}
