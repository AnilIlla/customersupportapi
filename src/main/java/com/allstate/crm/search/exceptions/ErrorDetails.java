package com.allstate.crm.search.exceptions;

import java.util.Date;

public class ErrorDetails {
    private Date timestamp;
    private String message;
    private String details;
    private int status;
    private String path;

    public ErrorDetails(Date timestamp, String message, String details, int status,String path) {
        super();
        this.timestamp = timestamp;
        this.message = message;
        this.details = details;
        this.status = status;
        this.path = path;
    }

    public String getPath() { return path; }

    public void setPath(String path) { this.path = path; }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getStatus() { return status; }

    public void setStatus(int status) { this.status = status; }
}
