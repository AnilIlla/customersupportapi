package com.allstate.crm.search.exceptions;

public class SearchServiceException extends RuntimeException {
    public SearchServiceException(String message) {
        super(message);
    }
}
