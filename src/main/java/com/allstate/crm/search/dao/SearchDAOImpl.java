package com.allstate.crm.search.dao;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.search.repository.CustomerMongoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class SearchDAOImpl implements SearchDAO {

    @Autowired
    CustomerMongoRepository customerMongoRepository;

    @Override
    public List<Customer> findCustomersByFirstName(String firstName) {
        return customerMongoRepository.findByFirstNameIgnoreCase(firstName);
    }

    @Override
    public List<Customer> findCustomersByPolicyNumber(String policyNumber) {
        return customerMongoRepository.findByPoliciesIn(Arrays.asList(policyNumber));
    }

    @Override
    public List<Customer> findCustomersByPhoneNumber(String phoneNumber) {
        return customerMongoRepository.findByPhone(phoneNumber);
    }

    @Override
    public List<Customer> findCustomersByEmail(String email) {
        return customerMongoRepository.findByEmailIgnoreCase(email);
    }

    @Override
    public int saveCustomer(Customer customer) {
        customerMongoRepository.save(customer);
        return 1;
    }
}
