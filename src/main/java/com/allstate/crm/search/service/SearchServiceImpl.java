package com.allstate.crm.search.service;

import com.allstate.crm.entities.Customer;
import com.allstate.crm.search.exceptions.InvalidServiceRequestException;
import com.allstate.crm.search.exceptions.SearchServiceException;
import com.allstate.crm.search.dao.SearchDAO;
import com.allstate.crm.utility.ValidationHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {

    Logger logger = LoggerFactory.getLogger(SearchServiceImpl.class);

    @Autowired
    private SearchDAO searchDAO;

    @Autowired
    private ValidationHelper validationHelper;

    public SearchServiceImpl() {
    }

    @Override
    public List<Customer> getCustomersByFirstName(String firstName) {
        logger.info("starting method getCustomersByFirstName with first name as " + firstName);
        List<Customer> customers = new ArrayList<Customer>();
        try {
            if (validationHelper.isEmptyString(firstName)) {
                throw new InvalidServiceRequestException("First Name Cannot be Null/Empty");
            } else {
                customers = searchDAO.findCustomersByFirstName(firstName);
            }
        } catch (InvalidServiceRequestException exception) {
            logger.error(exception.getMessage());
            throw new InvalidServiceRequestException(exception.getMessage());
        } catch (Exception exception) {
            logger.error(exception.getMessage());
            throw new SearchServiceException("Search Service Currently Experiencing Issues");
        }
        logger.info("ended method getCustomersByFirstName returned " + customers.toString());
        return customers;
    }

    @Override
    public List<Customer> getCustomersByPolicyNumber(String policyNumber) {
        logger.info("starting method getCustomersByPolicyNumber with policyNumber as " + policyNumber);
        List<Customer> customers = new ArrayList<Customer>();
        try {
            if (validationHelper.isEmptyString(policyNumber)) {
                throw new InvalidServiceRequestException("PolicyNumber Cannot be Null/Empty");
            } else {
                customers = searchDAO.findCustomersByPolicyNumber(policyNumber);
            }
        } catch (InvalidServiceRequestException exception) {
            logger.error(exception.getMessage());
            throw new InvalidServiceRequestException(exception.getMessage());
        } catch (Exception exception) {
            logger.error(exception.getMessage());
            throw new SearchServiceException("Search Service Currently Experiencing Issues");
        }
        logger.info("ended method getCustomersByPolicyNumber returned " + customers.toString());
        return customers;
    }

    @Override
    public List<Customer> getCustomersByPhoneNumber(String phoneNumber) {
        logger.info("starting method getCustomersByPhoneNumber with phoneNumber as " + phoneNumber);
        List<Customer> customers = new ArrayList<Customer>();
        try {
            if (validationHelper.isEmptyString(phoneNumber)) {
                throw new InvalidServiceRequestException("PhoneNumber Cannot be Null/Empty");
            } else {
                customers = searchDAO.findCustomersByPhoneNumber(phoneNumber);
            }
        } catch (InvalidServiceRequestException exception) {
            logger.error(exception.getMessage());
            throw new InvalidServiceRequestException(exception.getMessage());
        } catch (Exception exception) {
            logger.error(exception.getMessage());
            throw new SearchServiceException("Search Service Currently Experiencing Issues");
        }
        logger.info("ended method getCustomersByPhoneNumber returned " + customers.toString());
        return customers;
    }

    @Override
    public List<Customer> getCustomersByEmail(String email) {
        logger.info("starting method getCustomersByEmail with email as " + email);
        List<Customer> customers = new ArrayList<Customer>();
        try {
            if (validationHelper.isEmptyString(email) || !validationHelper.isValidEmail(email)) {
                throw new InvalidServiceRequestException("Invalid/Empty Email id");
            } else {
                customers = searchDAO.findCustomersByEmail(email);
            }
        } catch (InvalidServiceRequestException exception) {
            logger.error(exception.getMessage());
            throw new InvalidServiceRequestException(exception.getMessage());
        }
        catch (Exception exception) {
            logger.error(exception.getMessage());
            throw new SearchServiceException("Search Service Currently Experiencing Issues");
        }
        logger.info("ended method getCustomersByEmail returned " + customers.toString());
        return customers;
    }

    @Override
    public int saveCustomer(Customer customer) {
        searchDAO.saveCustomer(customer);
        return 1;
    }
}
