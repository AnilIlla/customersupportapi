package com.allstate.crm.rest;

import com.allstate.crm.entities.Interaction;
import com.allstate.crm.service.InteractionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*")
public class InteractionController  {

    Logger logger = LoggerFactory.getLogger(PolicyController.class);

    @Autowired
    private InteractionService service;

    public InteractionController()
    {

    }

    @RequestMapping(value = "/customer/interaction/{custId}", method = RequestMethod.GET)
    public List<Interaction> getInteracionsByCustId(@PathVariable("custId") int custId)
    {
        logger.info("starting method getInteracionsByCustId  " + custId);
        List<Interaction> interactionDetails;
        interactionDetails = service.getInteractionDetailByCustId(custId);
        logger.info("Interaction details fetched", interactionDetails);
        return interactionDetails;
    }
}
