package com.allstate.crm;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Value("${myconfig.timezone}")
    private  String timeZone;

    @Value("${myconfig.dateformat}")
    private  String dateformat;

    @Value("^(.+)@(.+)$")
    private String emailRegxPattern;

    public String getEmailRegxPattern() {
        return  emailRegxPattern;
    }

    public String getDateformat() {
        return dateformat;
    }

    public  String getTimeZone() {
        return timeZone;
    }

    public String getValidStringFormat() {
        return validStringFormat;
    }

    public void setValidStringFormat(String validStringFormat) {
        this.validStringFormat = validStringFormat;
    }

    @Value("^[\\w\\-\\s]+$")
    private String validStringFormat;
}
